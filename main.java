class Main
{
	public static void main(String args[])
	{
		StudentDataProvider stpr = new StudentDataProvider("students.txt"); //load data
		Student st = stpr.GetById(2);
		st.SetName("Vasya");
		stpr.Update(st);
		stpr.Save("students2.txt");
	}
}