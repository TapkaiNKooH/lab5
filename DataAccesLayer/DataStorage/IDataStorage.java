interface IDataStorage <TEntity>
{
	List<TEntity> data;
	void Load(string filename);
	void Save(string filename);
}




//////////////////////StudentDataStorage.java
class StudentDataStorage implements IDataStorage<Student>
{
	...
}