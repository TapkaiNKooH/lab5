public interface IDataProvider <TEntity>
{
	void Update(TEntity ent);
	void Delete(TEntity ent);
	//add, get by id, get all
}




/////////////////////////StudentDataProvider.java
class StudentDataProvider implements IDataProvider<Student>
{
	...
}
